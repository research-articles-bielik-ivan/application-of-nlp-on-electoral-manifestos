"""
The purpose of the script:

To output sentiment score using Vader Sentiment Analyser in every file
in "source" folder.

The output is in console.
"""

import os
import functions

files = os.listdir("source/")

for file in sorted(files):
    with open("./source/" + file, "r") as f:
        raw = f.read()
        clean = functions.clean_text(raw)

    total, pos, neg = functions.get_vader_sentiment(clean)
    print(file)
    print("Total: {}, Positive: {}, Negative: {}".format(total, pos, neg))
