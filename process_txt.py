"""
The purpose of the script:

To take a text file, define its language, and translate it into English.

The script accepts two arguments in command line.
1. definition of language of the text file
2. name of the text file that needs to be translated into English

Example usage:
python process_txt.py cs FILE.txt

The output is into a new text file called "results.txt"
in current working directory.
"""

import sys
from textblob import TextBlob

# first argument is language ISO code
lang = sys.argv[1]

# second argument is destination of file
fname = sys.argv[2]

with open(fname) as file:
    raw = file.read()
    text = TextBlob(raw).translate(from_lang=lang, to="en")


with open("result.txt", "w") as f:
    f.write(str(text))
