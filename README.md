# Application of Natural Language Processing on electoral manifestos of social democratic parties in CEE countries

This repository contains scripts used for the natural language processing tasks. I used these files to conduct an analysis on party manifestos of social democratic parties in Visegrad countries.  

The analysis is based on Python 3.

Results of the analysis are also stored in [OSF repository](https://osf.io/ndtqg) where source data, Python scripts, and results are located for academic purposes. The scripts stored on Gitlab work with the `source` folder that is on OSF. **The `source` folder needs to be downloaded to the working directory before running Python scripts.**

## Installing the virtual environment

In order to conduct the analysis, you have to first install virtual environment. **Requirements.txt** file is available in project repository and should be used to recreate the same environment as I used for the analysis.

The command is:

On Mac/Linux (you have to be in the project directory)

```
python3 -m venv env
source env/bin/activate
pip install -r requirements.txt
```

