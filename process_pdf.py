"""
This script takes a PDF document filename
and generate a text file from all of its text.

Needed module: PyMuPDF
https://github.com/pymupdf/PyMuPDF
! pip3 install PyMuPDF

The script takes one additional argument in command line
1. name of pdf file that needs to be transformed into text

Example usage:
python process_pdf.py FILE.pdf

The output is into a new text file called "test.txt"
in current working directory.
"""

import sys
import fitz

fname = sys.argv[1]
doc = fitz.open(fname)

with open("test.txt", "wb") as file:
    for page in doc:
        text = page.getText().encode("utf8")
        file.write(text)
        file.write(b"\n-----\n")    # write page delimiter
