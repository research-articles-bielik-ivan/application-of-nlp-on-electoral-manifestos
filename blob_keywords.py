"""
The purpose of the script:

To output sorted list of common noun phrases in every file
in "source" folder.

The output is in console.
"""

import os
from textblob import TextBlob
from pprint import pprint

files = os.listdir("source/")


def get_noun_counts(text):
    blob = TextBlob(text).replace("\n", " ")
    dict_nouns = blob.np_counts

    new_dict = {k: v for k, v in dict_nouns.items() if v >= 7}

    return sorted(new_dict.items(), key=lambda x: x[1], reverse=True)


for file in sorted(files):
    print(file)
    with open("./source/" + file) as f:
        raw = f.read()
        results = get_noun_counts(raw)
        pprint(results)
