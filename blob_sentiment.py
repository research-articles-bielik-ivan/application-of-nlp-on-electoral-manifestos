"""
The purpose of the script:

To output sentiment score in every text file in "source" folder.

The output is in console.
"""

import os
from textblob import TextBlob

files = os.listdir("source/")


def sentence_sentiment_analysis(file):
    with open("./source/" + file, "r") as f:
        raw = f.read()
        blob = TextBlob(raw)

    total_count = len(blob.sentences)
    positive_count = 0
    negative_count = 0

    for sentence in blob.sentences:
        if sentence.sentiment.polarity >= 0.25:
            positive_count += 1
        if sentence.sentiment.polarity <= -0.25:
            negative_count += 1

    return total_count, positive_count, negative_count


for f in sorted(files):
    total, pos, neg = sentence_sentiment_analysis(f)
    print(f)
    print("Total: {}, Positive: {}, Negative: {} \n".format(total, pos, neg))
