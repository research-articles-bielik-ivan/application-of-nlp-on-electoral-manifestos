"""
The purpose of the script:

To output 10 most significant keywords in every file
in "source" folder using RAKE algorithm.

The output is in console.
"""

import os
from functions import get_top10_keywords

files = os.listdir("source/")

for file in sorted(files):
    with open("./source/" + file, "r") as f:
        raw = f.read()
        results = get_top10_keywords(raw)
        print(file, results)
