
def clean_words(sentence):
    from nltk import word_tokenize
    from nltk.corpus import stopwords
    from nltk.stem import WordNetLemmatizer

    get_lemmas = WordNetLemmatizer()
    stop_words = stopwords.words("english")

    sentence = sentence.replace("\n", " ")
    words = word_tokenize(sentence)
    words = [w.lower() for w in words if w.isalpha()]
    words = [get_lemmas.lemmatize(w) for w in words if w not in stop_words]
    return ", ".join(words)


def clean_text(text):
    from nltk import sent_tokenize

    sents = sent_tokenize(text)
    return [clean_words(sent) for sent in sents]


def get_vader_sentiment(iterable):
    from nltk.sentiment.vader import SentimentIntensityAnalyzer
    sia = SentimentIntensityAnalyzer()

    pos_count = 0
    neg_count = 0
    total_count = len(iterable)

    for sent in iterable:
        sentiment = sia.polarity_scores(sent)
        if sentiment["compound"] >= 0.25:
            pos_count += 1
        if sentiment["compound"] <= -0.25:
            neg_count += 1

    return total_count, pos_count, neg_count


def get_top10_keywords(text):
    """
    Function opens a text file and extracts top 10 keywords
    based on RAKE alghorithm.

    Maximum length of a keyword is 3 words.
    Function also returns the score of the keyword in a tuple.
    """
    from rake_nltk import Rake

    # initialise rake object
    r = Rake(max_length=3)

    r.extract_keywords_from_text(text)
    # get keyword phrases ranked highest to lowest.
    list_of_keys = r.get_ranked_phrases_with_scores()

    return list_of_keys[:10]


def get_tokens(text):
    from nltk import word_tokenize
    from nltk.corpus import stopwords
    from nltk.stem import WordNetLemmatizer

    get_lemmas = WordNetLemmatizer()
    stop_words = stopwords.words("english")

    words = word_tokenize(text)
    words = [w.lower() for w in words if w.isalpha()]
    words = [get_lemmas.lemmatize(w) for w in words if w not in stop_words]
    return words
