"""
The purpose of the script:

To output 10 most common words in every file in "source" folder.

The output is in console.
"""

import os
import functions
from nltk.probability import FreqDist

files = os.listdir("source/")


for file in sorted(files):
    with open("./source/" + file, "r") as f:
        raw = f.read()
        clean = functions.get_tokens(raw)
        fdist = FreqDist(clean)
        print(file, fdist.most_common(10))
